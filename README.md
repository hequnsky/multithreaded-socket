# multithreaded-socket

#### 介绍
帮助你快速构建TCP连接,封装的非阻塞多线程Socket， 实现了TCP服务器(TCPServer)&TCP客户端(TCPClient)

#### 软件架构
1. Protocol 协议包, 仿 HTTP 协议包结构
2. Buff Socket 读写的缓冲区
3. BaseHandler 处理连接的基类
4. TCPServer TCP 服务器主类
5. TCPClietn TCP 客户端主类
6. utils 工具包

#### 安装教程

1.  放到 Python 包目录, 包文件重命名为 `multithreaded_socket`
```python
python安装目录\Lib\site-packages\multithreaded_socket
```
2.  直接放到项目文件夹中使用, 包文件重命名为 `multithreaded_socket`

#### 使用说明

1.  客户端
```python
# 导入包, 自行修改位置
from client import TCPClient
from handler import BaseHandler
from protocol import Protocol, TEXT_MESSAGE, FILE_MESSAGE, JSON_MESSAGE


class SimpleHandler(BaseHandler):

    def before_request_middleware(self, request: Protocol) -> bool:
        if request.header.get("test") and request.header.get("test") == "Test Header":
            return True
        return False

    def text_handler(self, request: Protocol):
        print("type: ", request.type)
        print("version: ", request.version)
        print("header: ", request.header)
        print(request.body.decode("utf-8"))

    def json_handler(self, request: Protocol):
        pass

    def file_handler(self, request: Protocol):
        pass


if __name__ == '__main__':
    client = TCPClient("127.0.0.1", 8848, handler_cls=SimpleHandler)
    client.connect()

    while client.is_run:
        data = input()

        if data == "exit":
            client.node_disconnection()
            break

        p = Protocol(TEXT_MESSAGE, data)
        p.set_header("test", "Test Header")

        client.send(p)
```
2.  服务器
```python
# 导入包, 自行修改位置
from server import TCPServer
from handler import BaseHandler
from protocol import Protocol, TEXT_MESSAGE, FILE_MESSAGE, JSON_MESSAGE


class SimpleHandler(BaseHandler):

    def before_request_middleware(self, request: Protocol) -> bool:
        if request.header.get("test") and request.header.get("test") == "Test Header":
            return True
        return False

    def text_handler(self, request: Protocol):
        print("type: ", request.type)
        print("version: ", request.version)
        print("header: ", request.header)
        print(request.body.decode("utf-8"))

    def json_handler(self, request: Protocol):
        pass

    def file_handler(self, request: Protocol):
        pass


if __name__ == '__main__':
    server = TCPServer("127.0.0.1", 8848, SimpleHandler)
    server.run_server()

    while server.is_run:
        data = input()

        if data == "exit":
            server.node_disconnection()
            break

        p = Protocol(TEXT_MESSAGE, data)
        p.set_header("test", "Test Header")
        server.send(p)
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request